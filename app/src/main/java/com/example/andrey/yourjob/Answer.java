package com.example.andrey.yourjob;

/**
 * Created by Andrey on 21.03.2017.
 */

public class Answer {

    String question;
    String answer;
    Double[] choice;

    public Answer(String _question, boolean _answer, Double[] _choice) {

        question = _question;
        choice = _choice;
        if (_answer){
            answer = "Да";
        }else{
            answer = "Нет";
        }
    }
}
