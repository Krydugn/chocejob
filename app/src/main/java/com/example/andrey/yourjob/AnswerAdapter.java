package com.example.andrey.yourjob;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Andrey on 21.03.2017.
 */

public class AnswerAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Answer> answer;

    AnswerAdapter(Context context, ArrayList<Answer> _answer) {
        ctx = context;
        answer = _answer;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return answer.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return answer.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_answer, parent, false);
        }

        Answer a = getProduct(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        ((TextView) view.findViewById(R.id.tvQuestion)).setText(a.question);
        ((TextView) view.findViewById(R.id.tvAnswer)).setText(a.answer);
        ((TextView) view.findViewById(R.id.tvProgrammer)).setText(a.choice[0].toString());
        ((TextView) view.findViewById(R.id.tvCook)).setText(a.choice[1].toString());
        ((TextView) view.findViewById(R.id.tvDriver)).setText(a.choice[2].toString());
        ((TextView) view.findViewById(R.id.tvTeacher)).setText(a.choice[3].toString());
        return view;
    }

    // товар по позиции
    Answer getProduct(int position) {
        return ((Answer) getItem(position));
    }

}

