package com.example.andrey.yourjob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvQuestion;
    TextView tvProgrammer;
    TextView tvCook;
    TextView tvDriver;
    TextView tvTeacher;
    Button btYes;
    Button btNo;
    Button btShowAnswer;
    ListView listAnswer;
    Double[][] bufChance;
    int numOfQuestion = 15;
    Map<String, Double> job = new HashMap<String, Double>();
    Map<String, Double> jobChance;
    Question[] question = new Question[numOfQuestion];
    ArrayList<Answer> answer = new ArrayList<Answer>();
    AnswerAdapter adapter;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        tvProgrammer = (TextView) findViewById(R.id.tvProgrammer);
        tvCook = (TextView) findViewById(R.id.tvCook);
        tvDriver = (TextView) findViewById(R.id.tvDriver);
        tvTeacher = (TextView) findViewById(R.id.tvTeacher);
        btYes = (Button) findViewById(R.id.btYes);
        btNo = (Button) findViewById(R.id.btNo);
        btShowAnswer = (Button) findViewById(R.id.btShowAnswer);
        btYes.setOnClickListener(this);
        btNo.setOnClickListener(this);
        btShowAnswer.setOnClickListener(this);
        listAnswer = (ListView) findViewById(R.id.listAnswer);

        bufChance = new Double[numOfQuestion][];

        setJob();
        setQuestion();
        showQuestion(count);
    }

    void sendAnswer(Question _question, boolean _choice) {
        calculation(_question, _choice, "программист");
        calculation(_question, _choice, "повар");
        calculation(_question, _choice, "машинист");
        calculation(_question, _choice, "учитель");
    }

    void calculation(Question _question, boolean _choice, String _who) {
        double B, BER;
        if (_choice) {
            B = _question.getAnswer().get(_who);
        } else {
            B = 1 - _question.getAnswer().get(_who);
        }
        BER = job.get(_who);
        job.put(_who, (B * BER) / ((B * BER) + (1 - B) * (1 - BER)));
    }

    void setJob() {
        for (int i = 0; i < 4; i++) {
            job.put(getResources().getStringArray(R.array.Job)[i], 0.5);
        }
    }

    void setQuestion() {
        bufChance[0] = new Double[]{0.8, 0.5, 0.9, 0.3};
        bufChance[1] = new Double[]{0.8, 0.3, 0.8, 0.9};
        bufChance[2] = new Double[]{0.5, 0.3, 0.9, 0.2};
        bufChance[3] = new Double[]{0.9, 0.2, 0.2, 0.4};
        bufChance[4] = new Double[]{0.8, 0.4, 0.4, 0.9};
        bufChance[5] = new Double[]{0.8, 0.7, 0.4, 0.5};
        bufChance[6] = new Double[]{0.9, 0.4, 0.3, 0.5};
        bufChance[7] = new Double[]{0.4, 0.9, 0.2, 0.6};
        bufChance[8] = new Double[]{0.3, 0.5, 0.2, 0.9};
        bufChance[9] = new Double[]{0.8, 0.5, 0.3, 0.7};
        bufChance[10] = new Double[]{0.5, 0.2, 0.9, 0.1};
        bufChance[11] = new Double[]{0.4, 0.9, 0.3, 0.4};
        bufChance[12] = new Double[]{0.3, 0.4, 0.5, 0.1};
        bufChance[13] = new Double[]{0.6, 0.6, 0.8, 0.4};
        bufChance[14] = new Double[]{0.3, 0.8, 0.2, 0.3};



        for (int i = 0; i < numOfQuestion; i++) {
            question[i] = new Question(getResources().getStringArray(R.array.Question)[i], setChance(bufChance[i]));
        }

    }

    Map<String, Double> setChance(Double[] _chance) {
        jobChance = new HashMap<String, Double>();
        for (int i = 0; i < 4; i++) {
            jobChance.put(getResources().getStringArray(R.array.Job)[i], _chance[i]);
        }
        return jobChance;
    }

    public void showQuestion(int _count) {
        tvQuestion.setText(question[_count].getDescription());

    }

    Double[] showJob() {
        Double chance;
        Double[] chanceArray = new Double[4];

        chance = Math.ceil((job.get("программист")) * 100.0);
        tvProgrammer.setText(chance.toString() + "%");
        chanceArray[0] = chance;
        chance = Math.ceil((job.get("повар")) * 100.0);
        tvCook.setText(chance.toString() + "%");
        chanceArray[1] = chance;
        chance = Math.ceil((job.get("машинист")) * 100.0);
        tvDriver.setText(chance.toString() + "%");
        chanceArray[2] = chance;
        chance = Math.ceil((job.get("учитель")) * 100.0);
        tvTeacher.setText(chance.toString() + "%");
        chanceArray[3] = chance;
        return chanceArray;
    }

    void click(boolean _choice) {
        if (count < numOfQuestion) {
            sendAnswer(question[count], _choice);
            answer.add(new Answer(question[count].getDescription(), _choice, showJob()));
            count++;
            if (count == numOfQuestion) {
                tvQuestion.setText("ВСЁ");
                btNo.setVisibility(View.GONE);
                btYes.setVisibility(View.GONE);
                btShowAnswer.setVisibility(View.VISIBLE);
            } else {
                showQuestion(count);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btYes:
                click(true);
                break;

            case R.id.btNo:
                click(false);
                break;

            case R.id.btShowAnswer:
                tvQuestion.setVisibility(View.GONE);
                btShowAnswer.setVisibility(View.GONE);
                listAnswer.setVisibility(View.VISIBLE);
                adapter = new AnswerAdapter(this, answer);
                listAnswer.setAdapter(adapter);
                break;
        }
    }
}
