package com.example.andrey.yourjob;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created by Andrey on 16.03.2017.
 */

 class Question {

    String description;
    private Map<String, Double> chanceYes = new HashMap<String, Double>();

    Question(String _description, Map<String, Double> _chanceYes) {

        description = _description;
        chanceYes = _chanceYes;
    }

    String getDescription() {
        return description;
    }

    Map<String, Double> getAnswer() {
        return chanceYes;

    }
}
